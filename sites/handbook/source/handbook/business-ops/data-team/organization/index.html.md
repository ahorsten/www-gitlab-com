---
layout: handbook-page-toc
title: "Data Team Organization"
description: "GitLab Data Team Organization"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

----

## <i class="far fa-compass fa-fw icon-color font-awesome" aria-hidden="true"></i> Team Organization 

### Data Analysts

The Data Analyst Team operates in a hub and spoke model, where some analysts are part of the central data team (hub) while others are embedded (spoke) or distributed (spoke) throughout the organization.

**Central** - those in this role report to and have their priorities set by the Data team. They currently support those in the Distributed role, cover ad-hoc requests, and support all Departments.

**Embedded** - those in this role report to the data team but their priorities are set by their Departments.

**Distributed** - those in this role report to and have their priorities set by their Departments. However, they work closely with those in the Central role to align on data initiatives and for assistance on the technology stack.

All roles mentioned above have their MRs and dashboards reviews by members in the Data team. Both Embedded and Distributed data analyst or data engineer tend to be subject matter experts (SME) for a particular business unit.

#### Data Support

| Role             | Team Member                                    | Fusion Team               | Timezone           |
| -------------    | ---------------------------------------------- | ------------------------- | ------------------ |
| Manager, Data    | [@kathleentam](https://gitlab.com/kathleentam) | [Release to Adoption](/handbook/business-ops/data-team/organization/index.html#release-to-adoption)            | US Eastern (UTC-4) |
| Jr. Data Analyst | [@ken_aguilar](https://gitlab.com/ken_aguilar) | [Release to Adoption](/handbook/business-ops/data-team/organization/index.html#release-to-adoption)            | Philippine (UTC+8) |
| Data Analyst     | [@iweeks](https://gitlab.com/iweeks)           | [Lead to Cash & Operations](/handbook/business-ops/data-team/organization/index.html#lead-to-cash--operations) | US Eastern (UTC-4) |
| Jr. Data Analyst | [@jeanpeguero](https://gitlab.com/jeanpeguero) | [Lead to Cash & Operations](/handbook/business-ops/data-team/organization/index.html#lead-to-cash--operations) | Atlantic   (UTC-4) |
| Data Analyst     | [@derekatwood](https://gitlab.com/derekatwood) | [Release to Adoption](/handbook/business-ops/data-team/organization/index.html#release-to-adoption)            | US Central (UTC-5) |
| Sr. Data Analyst | [@Pluthra](https://gitlab.com/Pluthra)         | [Lead to Cash & Operations](/handbook/business-ops/data-team/organization/index.html#lead-to-cash--operations) | US Central (UTC-5) |
| Sr. Data Analyst | [@mpeychet](https://gitlab.com/mpeychet)       | [Release to Adoption](/handbook/business-ops/data-team/organization/index.html#release-to-adoption)            | Euro Cent  (UTC+2) |


### Data Engineers

[Data Engineer Handbook](/handbook/business-ops/data-team/organization/engineering)

Though Data Engineers are sometimes given assignments to better support business functions **no members of the data engineering team are embedded or distributed**. This allows them to focus on our data platform with an appropriate development cadence.

| Role                 | Team Member                                          | Fusion Team               | Timezone               |
|----------------------|------------------------------------------------------|---------------------------|------------------------|
| Staff Data Engineer  | [@jjstark](https://gitlab.com/jjstark)               | [Lead to Cash & Operations](/handbook/business-ops/data-team/organization/index.html#lead-to-cash--operations)  | US Pacific (UTC-7)     |
| Staff Data Engineer  | [@tayloramurphy](https://gitlab.com/tayloramurphy)   | [Platform Enhancement](/handbook/business-ops/data-team/organization/index.html#data-platform-enhancement) | US Central (UTC-5)     |
| Senior Data Engineer | [@m_walker](https://gitlab.com/m_walker)             | [Release to Adoption & Platform Enhancement](/handbook/business-ops/data-team/organization/index.html#release-to-adoption)             | US Central (UTC-5)     |
| Data Engineer        | [@msendal](https://gitlab.com/msendal)               | [Lead to Cash & Operations](/handbook/business-ops/data-team/organization/index.html#lead-to-cash--operations)  | Central Europe (UTC+2) |
| Data Engineer        | [@paul_armstrong](https://gitlab.com/paul_armstrong) | [Lead to Cash & Operations](/handbook/business-ops/data-team/organization/index.html#lead-to-cash--operations)  | Central Africa (UTC+2) |

### Fusion Teams

The goal of our Data Fusion teams is to create **Business-Focused** and **Business-Involved** teams focused on GitLab’s most important business processes: lead-to-cash cycle and product-release-to-adoption as well as a Data Platform Enhancement team that focuses on iterating on the data platform supporting these business focused and involved teams. 

#### Lead to Cash & Operations

Mission: Deliver Data Solutions covering Lead-to-Cash, People, and Operations that contribute to GitLab’s success in public company readiness, financial predictability, and revenue journey goals. 

| Role | Team Member |
|---|---|
| Project [DRI](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/) | [@jjstark](https://gitlab.com/jjstark) |
| Data Analyst | [@iweeks](https://gitlab.com/iweeks) |
| Jr. Data Analyst | [@jeanpeguero](https://gitlab.com/jeanpeguero) |
| Sr. Data Analyst | [@Pluthra](https://gitlab.com/Pluthra) |
| Data Engineer | [@msendal](https://gitlab.com/msendal) |
| Data Engineer | [@paul_armstrong](https://gitlab.com/paul_armstrong) |
| Marketing Executive | Todd Barr |
| **Sales Executive** | Michael McBride |
| **Finance Executive** | Craig Mestel |

#### Release to Adoption

Mission: Deliver Data Solutions covering Product Release to Adoption, advance the state of product telemetry, product data integration with the Lead-to-Cash business processes.

| Role | Team Member |
|---|---|
| Project [DRI](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/) | [@kathleentam](https://gitlab.com/kathleentam) |
| Data Analyst | [@derekatwood](https://gitlab.com/derekatwood) |
| Sr. Data Analyst | [@mpeychet](https://gitlab.com/mpeychet) |
| Senior Data Engineer | [@m_walker](https://gitlab.com/m_walker) |
| **Engineering Executive** | Eric Johnson |
| **Product Executive** | Scott Williamson |

#### Data Platform Enhancement

Mission: Deliver standards, designs, references to enable successful delivery of Level 2 Solutions by Fusion Teams and provide coverage for strategic analytics, technology projects, and “flex” capacity as needed by Fusion Teams.

| Role | Team Member |
|---|---|
| Project [DRI](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/) | [@rparker2](https://gitlab.com/rparker2) |
| Staff Data Engineer  | [@tayloramurphy](https://gitlab.com/tayloramurphy) |
| Senior Data Engineer | [@m_walker](https://gitlab.com/m_walker) |
| **BizTech Executive** | Bryan Wise |
| **Chief of Staff** | Stella Treas |

---


## Job Descriptions
#### Data Analyst
[Job Family](/job-families/finance/data-analyst/){:.btn .btn-purple}

#### Data Engineer
[Job Family](/job-families/finance/data-engineer/){:.btn .btn-purple}

#### Manager
[Job Family](/job-families/finance/manager-data){:.btn .btn-purple}

#### Director of Data and Analytics
[Job Family](/job-families/finance/dir-data-and-analytics){:.btn .btn-purple}
