---
layout: handbook-page-toc
title: "Business Operations"
description: "Business Operations"
---
<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## <i class="far fa-paper-plane" id="biz-tech-icons"></i> How to reach out to us? 

<div class="flex-row" markdown="0">
  <div>
    <h5>Employment Enablement</h5>
    <a href="/handbook/business-ops/team-member-enablement/" class="btn btn-purple" style="width:170px;margin:5px;">Handbook page</a>
    <a href="/handbook/business-ops/team-member-enablement/#welcome-to-the-business-technology-employee-enablement-teamit-handbook-page" class="btn btn-purple" style="width:170px;margin:5px;">Contact the team</a>
  </div>
  <div>
    <h5>Enterprise Applications</h5>
    <a href="/handbook/business-ops/enterprise-applications/" class="btn btn-purple" style="width:170px;margin:5px;">Handbook page</a>
    <a href="https://gitlab.com/gitlab-com/business-ops/Business-Operations/-/issues/new" class="btn btn-purple" style="width:170px;margin:5px;">Issue tracker</a>
  </div>
</div>

<div class="flex-row" markdown="0">
  <div>
    <h5>Procurement</h5>
    <a href="/handbook/finance/procurement/" class="btn btn-purple" style="width:170px;margin:5px;">Handbook page</a>
    <a href="/handbook/finance/procurement/#how-do-i-contact-procurement" class="btn btn-purple" style="width:170px;margin:5px;">Contact the team</a>
  </div>
  <div>
    <h5>Data Team</h5>
    <a href="/handbook/business-ops/data-team" class="btn btn-purple" style="width:170px;margin:5px;">Handbook page</a>
    <a href="/handbook/business-ops/data-team/#contact-us" class="btn btn-purple" style="width:170px;margin:5px;">Contact the team</a>
  </div>
</div>


# <i class="far fa-newspaper" id="biz-tech-icons"></i> What's new?

- [Business Operations Hackathon - Jun 25th](https://gitlab.com/gitlab-com/business-ops/business-technology-hackathon/-/issues/1)
- [Endpoint management](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/48867)  


## <i class="fas fa-tasks" id="biz-tech-icons"></i> Objectives of our organization

<table id="objectives-table-bizops">
  <tr>
    <th>
        <i class="fas fa-users-cog"></i>
        <h5>Employee Productivity</h5>
    </th>
    <th>
        <i class="fas fa-user-clock"></i>
        <h5>Project Delivery & Business Outcomes</h5>
    </th>
    <th>
        <i class="fas fa-chart-line"></i>
        <h5>Data Driven Business </h5>
    </th>
    <th>
        <i class="fas fa-shield-alt"></i>
        <h5>Security</h5>
    </th>
  </tr>
  <tr>
      <td>
        <ul> 
            <li>Best in class employee experience by removing friction with technology</li>
            <li>Provide employee support model to maximize productivity</li>
            <li>Improve team on-boarding with automated orchestration</li>
            <li>Provide innovative solutions to promote the Remote only business model</li>
        </ul>
      </td>
      <td>
        <ul> 
            <li>Stand up Enterprise Applications function to manage and support Tier 1 tech stacks</li>
            <li>Align business priorities and IT roadmaps</li>
            <li>Create Program Management Maturity Model for IT</li>
            <li>Develop nimble IT project methodology to ensure health project delivery</li>
            <li>Develop Enterprise Architecture methodology</li>
        </ul>
      </td>
      <td>
        <ul> 
            <li>Create best in class data warehouse to make data driven decisions</li>
            <li>Scale Data Engineering to improve quality of data for high fidelity reporting</li>
            <li>Mature Data Analytics organization as a hybrid model of centralized technical data analysts and distributed business data analysts</li>
        </ul>
      </td>
      <td>
        <ul> 
            <li>Support Security Program to meet GitLab customer expectations</li>
            <li>Implement programs and systems to manage corporate assets and data</li>
            <li>Gain alignment and develop cross-functional operating model between Security and IT teams</li>
            <li>Develop IT Compliance program, framework, policies and audit controls</li>
        </ul>
      </td>
  </tr>
  
</table>




# <i class="fas fa-users" id="biz-tech-icons"></i>The team

## Reaching the Teams

- [How to work with Business Operations](/handbook/business-ops/how-we-work/)
 
- Groups in GitLab
    - `@gitlab-com/business-ops/bizops-IT-help`
    - `@gitlab-com/business-ops/bizops-bsa`
    - `@gitlab-com/business-ops`
    - `@gitlab-com/business-ops/financeops`
    - `@gitlab-com/business-ops/team-member-enablement`
    - `@gitlab-com/Finance-Division/procurement-team`
- Channels in Slack
    - `#business-operations`
    - `#bzo-business-engagement-team`
    - `#bt-finance-operations`
    - `#it_help`
    - `#procurement`

## Business Operations READMEs

Get to know the people who work in GitLab's Business Ops team by visiting our [READMEs](/handbook/business-ops/readmes/).

# Documentation
For reference, "[What Nobody Tells you about Documentation](https://www.divio.com/blog/documentation/)"
* Tutorials
* How-To Guides
* Explanation
* **Reference** (most of the information you can find in our handbook pages)
