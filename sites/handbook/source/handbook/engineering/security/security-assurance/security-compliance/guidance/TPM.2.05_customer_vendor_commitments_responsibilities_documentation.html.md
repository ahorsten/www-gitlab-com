---
layout: handbook-page-toc
title: "TPM.2.05 - Customer and Vendor Commitments and Responsibilities Documentation"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# TPM.2.05 - Customer and Vendor Commitments and Responsibilities Documentation

## Control Statement

Customer and vendor commitments and responsibilities are documented and signed upon customer or vendor onboarding.

## Context

## Scope

## Ownership

Control Owner:

* `Legal`

Process Owner:

* Procurement
* Business Ops

## Guidance


## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Vendor Non-Disclosure Agreement control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/927).

### Policy Reference
* [Legal Handbook page](/handbook/legal/)
* [Procure to Pay process](/handbook/finance/procure-to-pay/)
* [Uploading Vendor Contract into ContractWorks process](/handbook/legal/vendor-contract-filing-process/)
* [Contract types](/handbook/finance/procurement/#2-purchase-type-professional-services-and-all-other-contract-types)
* [NetSuite Vendor Report](https://app.periscopedata.com/app/gitlab/557709/Vendor-Reporting)
* [Process for modifying sales contracts](/handbook/legal/customer-negotiations/)
* [Process for negotiating terms](/handbook/business-ops/order-processing/#process-for-agreement-terms-negotiations-when-applicable-and-contacting-legal)

## Framework Mapping

* SOC
 * CC2.3
