---
layout: markdown_page
title: Product Direction - Telemetry
description: "Telemetry manages a variety of technologies that are important for GitLab's understanding of how our users use our products. Learn more here!"
canonical_path: "/direction/telemetry/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Direction for telemetry

Telemetry manages a variety of technologies that are important for GitLab's understanding of how our users use our products. These technologies include but are not limited to: Snowplow Analytics and an in-house tool called Usage Ping which is hosted on `version.gitlab.com` and includes a separate service called[ Version Check](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html#version-check-core-only).

If you'd like to discuss this vision directly with the Product Manager for Telemetry, feel free to reach out to Hila Qu via [e-mail](mailto:hilaqu@gitlab.com).

The primary purpose of telemetry is to help us build a better Gitlab. Data about how Gitlab is used is collected to better understand what parts of Gitlab needs improvement and what features to build next. Telemetry also helps our team better understand the reasons why people use Gitlab and with this knowledge we are able to make better product decisions.

The overall vision for the Telemetry group is to ensure that we have a robust, consistent and modern telemetry data framework in place to best serve our internal Product, Finance, Sales, and Customer Success teams. The group also ensures that GitLab has the best visualization and analysis tools in place that allows the best possible insights into the data provided through the various collection tools we utilize.

| Category | Description |
| ------ |  ------ |
| [🧺 Collection](/direction/telemetry/collection) | The structure(s) and platform(s) of how we collect Telemetry data |
| [🔍 Analysis](/direction/telemetry/analysis) | Manages GitLab's internal needs for an analysis tool that serves the Product department |

 
## Telemetry Guides 

To learn more about implementing telemetry, read our guides for Telemetry, Usage Ping, and Snowplow.

[Telemetry Guide](https://docs.gitlab.com/ee/development/telemetry/index.html)
- Our tracking tools
- What data can be tracked
- Telemetry systems overview

[Usage Ping Guide](https://docs.gitlab.com/ee/development/telemetry/usage_ping.html)
- What is Usage Ping
- Usage Ping payload
- Disabling Usage Ping
- Usage Ping request flow
- How Usage Ping works
- Implementing Usage Ping
- Developing and testing usage ping

[Snowplow Guide](https://docs.gitlab.com/ee/development/telemetry/snowplow.html)
- What is Snowplow
- Snowplow schema
- Enabling Snowplow
- Snowplow request flow
- Implementing Snowplow JS (Frontend) tracking
- Implementing Snowplow Ruby (Backend) tracking
- Developing and testing Snowplow

## Dashboards

Dashboards at GitLab have been created for the team to have better insights into how various parts of the company are performing. Today in Periscope we have dashboards that range from GitLab's overarching company KPIs, financial performance, Customer success, Sales forecasts to Product focused SMAU dashboards. 

### How to: Build Your  Own Dashboard in Periscope

Please reference the “[Self-Serve Analysis in Periscope](https://about.gitlab.com/handbook/business-ops/data-team/platform/periscope/#self-serve-analysis-in-periscope)” handbook page for a thorough step by step guide

Quick steps to get-started: 

*   The first step to building your own Periscope dashboard is checking that you have the correct permissions.
*   After logging in to Periscope using Okta, you should see a New Chart button in the top right corner. If you don’t see anything, you only have View-only access and you should follow the instructions above to gain Editor access.
*   Once you can see New Chart, you can start creating your own dashboards! Find Dashboards on the left side nav bar and click the + icon to build one. Make sure to give it a name in order to keep our directory organized.
*   Once your dashboard is built and named, you can start adding charts by clicking New Chart in the top right. Now you’re ready to start writing queries.

### How To: Request A Dashboard

*   To request a new dashboard or report please use [this link](https://gitlab.com/gitlab-data/analytics/issues/new). An issue will be added to the data team’s issue board. Please label the issue with ‘Growth’ and mention ‘Eli Kastelein’ and ‘Mathieu Peychet’ for visibility and prioritization.

### What do we have in place today and where are we headed?**

|  **Driver** | **GitLab.com** | **Self-Managed** |
| :---: | :---: | :---: |
|  % of Revenue | 10% | 90% |
|  MAU | ~750K | Millions (paid and CE ~5.5M) |
|  Ease of Data Collection | Easy | Complicated |
|  Data Sources Today | GitLab.com db<br/>Snowplow (basic) | Usage Ping |
|  Data Sources in Future | GitLab.com db<br/>Snowplow (enhanced) | Usage Ping|
|  Opt-out? | TBD | Yes |

## Priorities 

### Collection Priorities

#### Current focus

| Priority | Focus | Why? |
| :------: | ------ | ------ |
| 1️⃣ | [Enhance Usage Ping to Measure AMAU/SMAU](https://gitlab.com/gitlab-org/telemetry/issues/308) | After this epic is closed, we should be able to measure usage activity from Usage Ping and have an internally consistent view of that data between self-managed and .com.|

#### Next up

| Priority | Focus | Why? |
| :------: | ------ | ------ |
| 2️⃣ | [Harden Usage Ping](https://gitlab.com/gitlab-org/telemetry/-/issues/335) | As our organization grows, we require better data to inform our product, marketing, and sales team as they make decisions to grow the business and realize our strategic goals. Usage Ping is a key provider of that data but Usage Ping in it's current state is fragile. If another stage team adds a complex counter to Usage Ping, we need to ensure the entire Usage Ping does not break. This parent issue will serve as the aggregation of issues required to improve the performance and stability of Usage Ping, so we can have a world-class data platform at GitLab. |
| 3️⃣  |[Telemetry Documentation & Guides](https://gitlab.com/gitlab-org/telemetry/issues/307)|  It is important that as we roll out new changes and develop processes and workflows, we clearly and transparently document everything in a way that is easily discoverable and digestible by both GitLab team members and users/customers.  |

### Analysis Priorities

#### Current focus

| Priority | Focus | Why? |
| :------: | ------ | ------ |
| 1️⃣| [AMAU/SMAU Dashboards](https://gitlab.com/groups/gitlab-org/-/epics/1325) | Parallel to enabling the measurement of [AMAU/SMAU](https://gitlab.com/gitlab-org/telemetry/issues/308), it's important that we are capturing the right metrics for each stage so that each Product Manager has visibility into how users are using their stage and stage categories. |


#### Next up

| Priority | Focus | Why? |
| :------: | ------ | ------ |
| 2️⃣ |[Explore Alternate Analytics Tools for Product](https://gitlab.com/gitlab-org/telemetry/issues/303)|  Product team members have a hard time having their needs met by Periscope, there is a need for the data to be explored easily without the need for SQL or getting a dashboard built. |


## How we prioritize

We follow the same prioritization guidelines as the [product team at large](https://about.gitlab.com/handbook/product/product-processes/#how-we-prioritize-work). Issues tend to flow from having no milestone, to being added to the backlog, to a directional milestone (e.g. Next 3-4 releases), and are finally assigned a specific milestone.

Our entire public backlog for Telemetry can be viewed [here](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=group%3A%3Atelemetry&scope=all&sort=popularity&state=opened&utf8=%E2%9C%93), and can be filtered by labels or milestones. If you find something you are interested in, you're encouraged to jump into the conversation and participate. At GitLab, everyone can contribute!
